'''
Created on 2018年4月26日

@author: tngou@tngou.net
'''

import urllib
from urllib import request
from http import cookiejar

header = {
"Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
"Accept-Encoding":"utf-8",
"Accept-Language":"zh-cn,zh;q=0.8,en-us;q=0.5,en;q=0.3",
"Connection":"keep-alive",
"User-Agent":"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:32.0) Gecko/20100101 Firefox/32.0"
}
def login(username,password):
    url = "https://passport.51jingying.com/login.php?act=formLogin"
    postdata =urllib.parse.urlencode({
            "username":username,
             "userpwd":password,
             "checked":1,
             "randomcode":316789
    }).encode('utf-8')

    req = request.Request(url,postdata,header)
    filename = 'cookie.txt'
    cookie = cookiejar.MozillaCookieJar(filename)
# cookie = http.cookiejar.CookieJar()
    opener = request.build_opener(request.HTTPCookieProcessor(cookie))
    r = opener.open(req)
    cookie.save(ignore_discard=True, ignore_expires=True)
    return r

def getHtml(url):
    #设置保存cookie的文件的文件名,相对路径,也就是同级目录下
    filename = 'cookie.txt'
    #创建MozillaCookieJar实例对象
    cookie = cookiejar.MozillaCookieJar()
    #从文件中读取cookie内容到变量
    cookie.load(filename, ignore_discard=True, ignore_expires=True)
    #利用urllib.request库的HTTPCookieProcessor对象来创建cookie处理器,也就CookieHandler
    handler=request.HTTPCookieProcessor(cookie)
    #通过CookieHandler创建opener
    opener = request.build_opener(handler)
    #此用opener的open方法打开网页
    r = opener.open(url)  
    return r
          
if __name__ == "__main__":   
#     r=login('phone_15662224934','0116abcd')
#     print(r.read().decode('gbk'))
    url = "https://www.51jingying.com/manager/cv.php?act=showCv&managerId=1576162576&caseid=0&isenglish=0&passkey=3bcecfeb82c9eb3b1dd98b690bb5aa60&source=foxengine"
    response=getHtml(url);
    f = open("r.html",'w') 
    f.write(response.read().decode('gbk')) 
    f.close()
